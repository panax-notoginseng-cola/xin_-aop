package com.xin.aop02.service.impl;

import org.springframework.stereotype.Service;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/11 17:21
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class OrderServiceImpl   {

    public void save() {
        System.out.println("保存订单");
    }
}