package com.xin.aop03.service.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/13 11:22
 * @description：
 * @modified By：
 * @version:
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyPermission {

    String value() default "";
}