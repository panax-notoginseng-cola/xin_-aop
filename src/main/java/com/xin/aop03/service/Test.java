package com.xin.aop03.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/11 21:59
 * @description：
 *
 *           案例1-使用Aop模拟事务管理
 *
 * @modified By：
 * @version:
 */
public class Test {
    public static void main(String[] args) {
        //读取spring—aspects配置文件
        ApplicationContext app = new ClassPathXmlApplicationContext("spring-aspects.xml");
        //该类使用了接口---使用jdk动态代理
        MyUserService myUserService = app.getBean(MyUserService.class);
        myUserService.save();
        // myUserService.update();

        //该类没有实现接口---切面使用cglib动态代理
        // OrderServiceImpl orderService = app.getBean(OrderServiceImpl.class);
        // orderService.save();

    }
}