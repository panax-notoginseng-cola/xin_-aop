package com.xin.aop03.service.controller;

import com.xin.aop03.service.annotation.MyPermission;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/13 11:25
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @PostMapping("/list")
    @MyPermission("list")
    public String list(){
        System.out.println("查询用户");
        return "list";
    }

    @PostMapping("/save")
    @MyPermission("save")
    public String save(){
        System.out.println("保存用户");
        return "save";
    }
}