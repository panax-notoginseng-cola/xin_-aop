package com.xin.aop03.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/13 10:38
 * @description：
 *
 *       案例2--使用注解切点
 *          修改spring配置文件
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/aop")
public class HelloController {

    @GetMapping("/getTest")
    public String getTest(){
        System.out.println("===========getTest==========");
        return "getTest";
    }
    @PostMapping("/postTest")
    public String postTest(){
        System.out.println("===========postTest==========");
        return "postTest ";
    }
}