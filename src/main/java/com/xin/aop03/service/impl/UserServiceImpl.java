package com.xin.aop03.service.impl;

import com.xin.aop03.service.MyUserService;
import org.springframework.stereotype.Service;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/11 17:19
 * @description：
 * @modified By：
 * @version:
 */
//该类实现了接口模式
@Service
public class UserServiceImpl implements MyUserService {
    @Override
    public void save() {
        System.out.println("保存用户");
        //为了演示异常通知
        // int a = 10/0;
    }

    @Override
    public void update() {
        System.out.println("修改用户");
    }
}