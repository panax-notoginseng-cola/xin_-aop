package com.xin.aop03.service.aspect;

import com.xin.aop03.service.annotation.MyPermission;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/13 11:30
 * @description：
 *          3. 案例2--自定义注解
 * @modified By：
 * @version:
 */
@Component
@Aspect
public class PermissionAdvice {

    //定义切点
    @Pointcut("@annotation(com.xin.aop03.service.annotation.MyPermission)")
    public void permissionPointcut(){
    }

    // @Before("permissionPointcut()")
    // public void before(){
    //     System.out.println("校验用户权限");
    // }

    private List<String> permission = new ArrayList<>();
    public PermissionAdvice(){
        permission.add("list");
        permission.add("delete");
        permission.add("update");
    }

    @Around("permissionPointcut()")
    public Object before(ProceedingJoinPoint joinPoint) throws Throwable {
        //得到连接点执行的方法对象
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        //方法上的注解对象
        MyPermission annotation = method.getAnnotation(MyPermission.class);

        if(annotation != null){
            //获取注解的属性value值
            String value = annotation.value();
            if(!permission.contains(value)){
                return "quanxianbuzou";
            }
        }

        return joinPoint.proceed();
    }
}