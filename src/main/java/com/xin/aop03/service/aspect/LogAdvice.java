package com.xin.aop03.service.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/13 10:41
 * @description：
 * @modified By：
 * @version:
 */
@Component
@Aspect
public class LogAdvice {

    //使用注解作为切点---后面指定哪个注解
    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
    private void logJoinPoint(){
    }

    @Before("logJoinPoint()")
    public void before(){
        System.out.println("添加getTest的前置日志");
    }
}