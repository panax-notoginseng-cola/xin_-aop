package com.xin.aop01.service;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/11 17:13
 * @description：
 * @modified By：
 * @version:
 */
public interface MyUserService {

    public abstract void save();

    public abstract void update();
}