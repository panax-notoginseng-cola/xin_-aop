package com.xin.aop01.service.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author ：Student王心
 * @date ：Created in 2022/11/11 17:29
 * @description：
 * @modified By：
 * @version:
 */
@Component //交于spring容器来创建该类对象
@Aspect //标记该类为切面类
public class TransactionAop {

    // @Pointcut("execution(public void com.xin.aop01.service.impl.UserServiceImpl.save(..))")
    // private void transationAdvice(){}
    /**
     * 第一个*:表示访问修饰符以及返回类型
     * 第二个*:表示任意类
     * 第三个*:表示任意方法
     *(..):表示任意参数
     */
    @Pointcut("execution(* com.xin.aop01.service.impl.*.*(..))")
    private void transationAdvice(){}


    // @Before("transationAdvice()")
    // public void before(){
    //     System.out.println("前置通知---开启事务");
    // }
    //
    //
    // @After("transationAdvice()")
    // public void after(){
    //     //理解为UserServiceImpl中的方法执行完
    //     System.out.println("后置通知---提交事务");
    // }
    //
    // @AfterReturning("transationAdvice()")
    // public void afterReturning(){
    //     System.out.println("后置返回通知");
    // }
    //
    // @AfterThrowing("transationAdvice()")
    // public void afterThrowing(){
    //     System.out.println("异常通知---回滚事务");
    // }

    @Around("transationAdvice()")
    public void around(ProceedingJoinPoint joinPoint){
        System.out.println("环绕通知---前");
        try {
            //回调连接点方法
            joinPoint.proceed();
            System.out.println("方法执行完毕");
        } catch (Throwable throwable) {
            System.out.println("环绕通知---异常");
        }finally {
            System.out.println("环绕通知---最终");
        }
    }
}